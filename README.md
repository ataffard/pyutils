pyUtils for usfull everyday ROOT stuff
========================================


To checkout the code

```
cd <Project Path>
git clone ssh://git@gitlab.cern.ch:7999/ataffard/pyUtils.git pyUtils

```

Then in your python code

```
WORKDIR = '<Project Path>'
sys.path.append(WORKDIR)

import pyUtils.plots.plot_utils as pu
import pyUtils.os.utils as utils

```

