# -*- coding: utf-8 -*-
# @Author: Anyes Taffard
# @Date:   2022-02-03 12:17:42
# @Last Modified by:   Anyes Taffard
# @Last Modified time: 2022-02-03 13:38:34
from setuptools import setup, find_packages

setup(
    name="pyUtils",
    version="0.1",
    description="",
    long_description="Python module with generic utilities =",
    url="",
    author="Anyes Taffard",
    author_email="ataffard@cern.ch",
    packages=find_packages(),
    include_package_data=True,
    python_requires=">=3.6",
    install_requires=["requests"],
)
