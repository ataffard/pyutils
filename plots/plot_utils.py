# -*- coding: utf-8 -*-
# @Author: Anyes Taffard
# @Date:   2022-02-03 13:31:19
# @Last Modified by:   Anyes Taffard
# @Last Modified time: 2024-05-01 17:32:47
#!/usr/bin/env python3
import ROOT
from math import sqrt
import array
import sys

import pyUtils.plots.plot as plot

ROOT.gStyle.SetCanvasPreferGL(ROOT.kTRUE)


# ROOT.TH1F.__init__._creates = False
# ROOT.TH2F.__init__._creates = False
# ROOT.TCanvas.__init__._creates = False
# ROOT.TPad.__init__._creates = False
# ROOT.TLine.__init__._creates = False
# ROOT.TLegend.__init__._creates = False
# ROOT.TGraphErrors.__init__._creates = False
# ROOT.TGraphAsymmErrors.__init__._creates = False
# ROOT.TLatex.__init__._creates = False
# ROOT.TFile.__init__._creates = False


# ----------------------------------------------
#  ATLAS label
# ----------------------------------------------
def ATLASLabel(x, y, color, opt=1):
    # delx = 0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw())

    l = ROOT.TLatex()
    # l.SetTextAlign(12) l.SetTextSize(tsize)
    l.SetNDC()
    l.SetTextFont(72)
    l.SetTextColor(color)
    l.DrawLatex(x, y, "ATLAS")

    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(42)
    p.SetTextColor(color)
    if opt == 1:
        p.DrawLatex(x + 0.1, y, " Internal")
    elif opt == 2:
        p.DrawLatex(x + 0.1, y, " Preliminary")
    elif opt == 3:
        p.DrawLatex(x + 0.1, y, " Simulation")


# ----------------------------------------------
#  Text Box
# ----------------------------------------------
def textBox(x, y, color, text, tsize):
    l = ROOT.TLatex()
    l.SetTextSize(tsize)
    l.SetNDC()
    l.SetTextColor(color)
    l.DrawLatex(x, y, text)


# ----------------------------------------------
#  TCanvas Methods
# ----------------------------------------------
def basic_canvas(name="c", width=768, height=768, nxpads=1, nypads=1):
    """
    Book a canvas and return it
    """
    c = ROOT.TCanvas(name, name, width, height)
    c.Divide(nxpads, nypads)
    c.cd(1)
    c.Modified()
    return c


def ratio_canvas(name="c"):
    ratio_can = plot.RatioCanvas(name)
    return ratio_can


# ----------------------------------------------
#  Retreive histo from file
# ----------------------------------------------
def getHisto(file, name, moveUnderFlow=True, moveOverFlow=True):

    h = file.Get(name).Clone()
    if h is None:
        print("ERROR: cannot find %s " % name)
        sys.exit(0)
    else:
        if moveUnderFlow:
            add_underflow_to_firstbin(h)
        if moveOverFlow:
            add_overflow_to_lastbin(h)
        return h


# ----------------------------------------------
#  TH1F Methods
# ----------------------------------------------
def th1f(name, title, nbin, nlow, nhigh, xtitle, ytitle="nEntries/bin"):
    """
    Book a TH1F and return it
    """
    h = ROOT.TH1F(name, title, nbin, nlow, nhigh)
    h.SetFillColorAlpha(ROOT.kRed, 20)
    font = 42
    tsize = 0.03
    h.SetTitleFont(font)
    h.SetTitleSize(tsize)

    # x-axis
    xaxis = h.GetXaxis()
    xaxis.SetTitle(xtitle)
    xaxis.SetTitleOffset(1.0 * xaxis.GetTitleOffset())
    xaxis.SetTitleFont(font)
    xaxis.SetLabelFont(font)

    # y-axis
    yaxis = h.GetYaxis()
    yaxis.SetTitle(ytitle)
    yaxis.SetTitleOffset(1.0 * yaxis.GetTitleOffset())
    yaxis.SetTitleFont(font)
    yaxis.SetLabelFont(font)

    # 2 is better than 1
    h.Sumw2()
    return h


# ----------------------------------------------
#  Add overflow to last bin
# ----------------------------------------------
def add_overflow_to_lastbin(hist):
    """
    Given an input histogram, add the overflow
    to the last visible bin
    """
    # find the last bin
    ilast = hist.GetXaxis().GetNbins()

    # read in the values
    lastBinValue = hist.GetBinContent(ilast)
    lastBinError = hist.GetBinError(ilast)
    overFlowValue = hist.GetBinContent(ilast + 1)
    overFlowError = hist.GetBinError(ilast + 1)

    # set the values
    hist.SetBinContent(ilast + 1, 0)
    hist.SetBinError(ilast + 1, 0)
    hist.SetBinContent(ilast, lastBinValue + overFlowValue)
    hist.SetBinError(ilast, sqrt(lastBinError * lastBinError + overFlowError * overFlowError))


# ----------------------------------------------
#  Add underflow to last bin
# ----------------------------------------------
def add_underflow_to_firstbin(hist):
    """
    Given an input histogram, add the underflow
    to the first visible bin
    """
    # find the last bin
    ifirst = 1

    # read in the values
    firstBinValue = hist.GetBinContent(ifirst)
    firstBinError = hist.GetBinError(ifirst)
    underflowValue = hist.GetBinContent(ifirst - 1)
    underflowError = hist.GetBinError(ifirst - 1)

    # set the values
    hist.SetBinContent(ifirst - 1, 0)
    hist.SetBinError(ifirst - 1, 0)
    hist.SetBinContent(ifirst, firstBinValue + underflowValue)
    hist.SetBinError(ifirst, sqrt(firstBinError * firstBinError + underflowError * underflowError))


# ----------------------------------------------
#  Scale histo to val
# ----------------------------------------------
def scale_hist(h, val, useUnderOver=True):
    if useUnderOver:
        integral = h.Integral(0, -1)
    else:
        integral = h.Integral(1, h.GetXaxis().GetNbins())
    if integral != 0:
        h.Scale(val / integral)


# ----------------------------------------------
#  Divide 2 histograms and return a TGraphAsymmErrors
# ----------------------------------------------
def divide_histograms(hnum, hden, xtitle, ytitle):
    """
    Provide two histograms and divide hnum/hden.
    Converts the final result into a tgraph.
    """
    nbins = hnum.GetNbinsX()
    # xlow = hnum.GetBinCenter(1)
    # xhigh = hnum.GetBinCenter(nbins+1)
    hratio = hnum.Clone("hratio")
    hratio.GetYaxis().SetTitle(ytitle)
    hratio.GetXaxis().SetTitle(xtitle)

    hratio.GetYaxis().SetTitleOffset(0.45 * hratio.GetYaxis().GetTitleOffset())
    hratio.GetYaxis().SetLabelSize(2 * hratio.GetYaxis().GetLabelSize())
    hratio.GetYaxis().SetTitleFont(42)
    hratio.GetYaxis().SetTitleSize(0.09)

    hratio.GetXaxis().SetTitleOffset(1.75 * hratio.GetYaxis().GetTitleOffset())
    hratio.GetXaxis().SetLabelSize(3 * hratio.GetXaxis().GetLabelSize())
    hratio.GetXaxis().SetTitleSize(0.15)
    hratio.GetXaxis().SetTitleFont(42)

    g = ROOT.TGraphAsymmErrors()
    gdata = th1_to_tgraph(hnum)
    for i in range(1, nbins + 1):
        c1 = float(hnum.GetBinContent(i))
        c2 = float(hden.GetBinContent(i))
        if c2 == 0:
            continue
        c3 = c1 / c2 * 1.0
        if c3 == 0:
            continue
        # if c3 == 0 : c3 = -99
        hratio.SetBinContent(i, c3)

        g.SetPoint(i - 1, hnum.GetBinCenter(i), c3)
        error_up = ((c2 + gdata.GetErrorYhigh(i - 1)) / c2) - 1.0
        error_dn = 1.0 - ((c2 - gdata.GetErrorYlow(i - 1)) / c2)
        g.SetPointError(i - 1, 0.5 * hratio.GetBinWidth(i), 0.5 * hratio.GetBinWidth(i), error_dn, error_up)
    return g


# ----------------------------------------------
#  Stack Method
# ----------------------------------------------
def makeStack(histos, hnames, legend):

    stack = ROOT.THStack("stack_" + histos[0].GetTitle(), "")
    stack.Clear()
    stack.SetTitle("stack_" + histos[0].GetTitle())
    stack.SetName("stack_" + histos[0].GetTitle())

    for i, h in enumerate(histos):
        stack.Add(h)
        legend.AddEntry(h, hnames[i], "f")

    return stack


# ----------------------------------------------
#  Set Stack axis etc... to be call after Draw
# ----------------------------------------------
def setStack(stack, h):

    stack.GetXaxis().SetTitle(h.GetXaxis().GetTitle())
    stack.GetYaxis().SetTitle(h.GetYaxis().GetTitle())


# ----------------------------------------------
#  TH1->TGraph Method
# ----------------------------------------------
def th1_to_tgraph(hist):
    """
    The provided histogram is turned into a TGraphErrors object
    """

    g = ROOT.TGraphAsymmErrors()

    # don't care about the underflow/overflow
    for ibin in range(1, hist.GetNbinsX() + 1):
        y = hist.GetBinContent(ibin)
        ey = hist.GetBinError(ibin)
        x = hist.GetBinCenter(ibin)
        ex = hist.GetBinWidth(ibin) / 2.0
        g.SetPoint(ibin - 1, x, y)
        g.SetPointError(ibin - 1, ex, ex, ey, ey)

    return g


# ----------------------------------------------
#  Convert to Poisson Errors
# ----------------------------------------------
def convert_errors_to_poisson(hist):
    """
    Provided a histogram, convert the errors
    to Poisson errors
    """
    # needed variables
    alpha = 0.158655
    beta = 0.158655

    g = ROOT.TGraphAsymmErrors()

    for ibin in range(1, hist.GetNbinsX() + 1):
        value = hist.GetBinContent(ibin)
        if value != 0:
            error_poisson_up = 0.5 * ROOT.TMath.ChisquareQuantile(1 - beta, 2 * (value + 1)) - value
            error_poisson_down = value - 0.5 * ROOT.TMath.ChisquareQuantile(alpha, 2 * value)
            ex = hist.GetBinWidth(ibin) / 2.0
            g.SetPoint(ibin - 1, hist.GetBinCenter(ibin), value)
            g.SetPointError(ibin - 1, ex, ex, error_poisson_down, error_poisson_up)
        else:
            g.SetPoint(ibin - 1, hist.GetBinCenter(ibin), 0.0)
            g.SetPointError(ibin - 1, 0.0, 0.0, 0.0, 0.0)

    return g


# ----------------------------------------------
#  Divide 2 TGraphs
# ----------------------------------------------
def tgraphErrors_divide(g1, g2):
    """
    Provided two TGraphErrors objects, divide them
    and return the resulting TGraphErrors object
    """
    n1 = g1.GetN()
    n2 = g2.GetN()
    if n1 != n2:
        print("traphErrors_divide ERROR    input TGraphErrors do not have same number of entries!")
    g3 = ROOT.TGraphErrors()

    iv = 0
    for i1 in range(n1):
        for i2 in range(n2):
            x1 = ROOT.Double(0.0)
            y1 = ROOT.Double(0.0)
            x2 = ROOT.Double(0.0)
            y2 = ROOT.Double(0.0)
            dx1 = ROOT.Double(0.0)
            dy1 = ROOT.Double(0.0)
            dy2 = ROOT.Double(0.0)

            g1.GetPoint(i1, x1, y1)
            g2.GetPoint(i2, x2, y2)

            if x1 != x2:
                continue
            # print "test"
            else:
                dx1 = g1.GetErrorX(i1)
                if y1 != 0:
                    dy1 = g1.GetErrorY(i1) / y1
                if y2 != 0:
                    dy2 = g2.GetErrorY(i2) / y2

                if y1 == 0.0:
                    g3.SetPoint(iv, x1, -10)  # if the ratio is zero, don't draw point at zero (looks bad on ratio pad)
                elif y2 != 0.0:
                    g3.SetPoint(iv, x1, y1 / y2)
                else:
                    g3.SetPoint(iv, x1, y2)

            e = ROOT.Double(0.0)

            if y1 != 0 and y2 != 0:
                e = sqrt(dy1 * dy1 + dy2 * dy2) * (y1 / y2)
            g3.SetPointError(iv, dx1, e)

            iv += 1

    return g3


# ----------------------------------------------
#  Divide 2 TGraphAsymmErrors
# ----------------------------------------------
def tgraphAsymmErrors_divide(g_num, g_den):
    n_num = g_num.GetN()
    n_den = g_den.GetN()
    if n_num != n_den:
        print("tgraphAsymmErrors_divide ERROR    input TGraphs do not have same number of entries!")
    g3 = ROOT.TGraphAsymmErrors()

    iv = 0
    for inum in range(n_num):
        for iden in range(n_den):
            x_num = ROOT.Double(0.0)
            y_num = ROOT.Double(0.0)
            x_den = ROOT.Double(0.0)
            y_den = ROOT.Double(0.0)

            ex = ROOT.Double(0.0)
            ey_num_up = ROOT.Double(0.0)
            ey_num_dn = ROOT.Double(0.0)
            ey_den_up = ROOT.Double(0.0)
            ey_den_dn = ROOT.Double(0.0)

            g_num.GetPoint(inum, x_num, y_num)
            g_den.GetPoint(iden, x_den, y_den)

            if x_num != x_den:
                continue
            else:
                if y_num != 0:
                    ey_num_up = g_num.GetErrorYhigh(inum) / y_num
                    ey_num_dn = g_num.GetErrorYlow(inum) / y_num
                if y_den != 0:
                    ey_den_up = g_den.GetErrorYhigh(iden) / y_den
                    ey_den_dn = g_den.GetErrorYlow(iden) / y_den

                if y_num == 0.0:
                    g3.SetPoint(iv, x_num, -10)
                elif y_den != 0:
                    g3.SetPoint(iv, x_num, y_num / y_den)
                else:
                    g3.SetPoint(iv, y_num, y_den)
            ex = g_num.GetErrorX(iv)

            e_up = ROOT.Double(0.0)
            e_dn = ROOT.Double(0.0)
            if y_num != 0 and y_den != 0:
                e_up = sqrt(ey_num_up * ey_num_up + ey_den_up * ey_den_up) * (y_num / y_den)
                e_dn = sqrt(ey_num_dn * ey_num_dn + ey_den_dn * ey_den_dn) * (y_num / y_den)
            g3.SetPointError(iv, ex, ex, e_dn, e_up)

            iv += 1
    return g3


# ----------------------------------------------
#  Build a Ratio Error Band
# ----------------------------------------------
def buildRatioErrorBand(g_in, g_out):
    g_out.SetMarkerSize(0)
    for bin in range(g_out.GetN()):
        y_out = ROOT.Double(1.0)
        x_out = ROOT.Double(0.0)
        y_in = ROOT.Double(0.0)
        x_in = ROOT.Double(0.0)

        g_in.GetPoint(bin, x_in, y_in)
        g_out.SetPoint(bin, x_out, y_out)

        # set upper error
        if y_in > 0.0001:
            g_out.SetPointEYhigh(bin, g_in.GetErrorYhigh(bin) / y_in)
        # g_out.GetErrorYhigh(bin) = g_in.GetErrorYhigh(bin) / y_in
        else:
            g_out.SetPointEYhigh(bin, 0.0)
        # g_out.GetErrorYhigh(bin) = 0.0

        # set lower error
        if y_in > 0.0001:
            g_out.SetPointEYlow(bin, g_in.GetErrorYlow(bin) / y_in)
            # g_out.GetErrorYow(bin) = g_in.GetErrorYlow(bin) / y_in
        else:
            g_out.SetPointEYlow(bin, 0.0)
            # g_out.GetErrorYlow(bin) = 0.0

        if g_out.GetErrorYlow(bin) > 1.0:
            g_out.SetPointEYlow(bin, 1.0)
            # g_out.GetErrorYlow(bin) = 1.
        if g_out.GetErrorYhigh(bin) > 1.0:
            g_out.SetPointEYhigh(bin, 1.0)
            # g_out.GetErrorYhigh(bin) = 1.


# ----------------------------------------------
#  Add band TGraph to another TGraph
# ----------------------------------------------
def add_to_band(g1, g2):  # , sys_name) :

    if g1.GetN() != g2.GetN():
        print("plot_utils::add_to_band WARNING    input graphs do not have the same number of points!")

    #  eyhigh = ROOT.Double(0.0)
    #  eylow  = ROOT.Double(0.0)

    #  x1 = ROOT.Double(0.0)
    #  y1 = ROOT.Double(0.0)
    #  y2 = ROOT.Double(0.0)
    #  y0 = ROOT.Double(0.0)

    for i in range(g1.GetN()):
        eyhigh = ROOT.Double(0.0)
        eylow = ROOT.Double(0.0)
        eyhigh = g2.GetErrorYhigh(i)
        eylow = g2.GetErrorYlow(i)

        x1 = ROOT.Double(0.0)
        y1 = ROOT.Double(0.0)
        y2 = ROOT.Double(0.0)
        y0 = ROOT.Double(0.0)

        g1.GetPoint(i, x1, y1)
        g2.GetPoint(i, x1, y2)

        if y1 == 0:
            y1 = 1
        if y2 == 0:
            y2 = 1

        eyh = ROOT.Double(0.0)
        eyl = ROOT.Double(0.0)

        y0 = y1 - y2
        # print "    > y0 : ", y0
        if y0 != 0:
            if y0 > 0:
                eyh = eyhigh
                eyh = sqrt(eyh * eyh + y0 * y0)
                # print "    > %s + "%sys_name, eyh
                g2.SetPointEYhigh(i, eyh)
            else:
                eyl = eylow
                eyl = sqrt(eyl * eyl + y0 * y0)
                # print "    > %s - "%sys_name, eyl
                g2.SetPointEYlow(i, eyl)


# ----------------------------------------------
#  TH2F Methods
# ----------------------------------------------
def th2f(name, title, nxbin, xlow, xhigh, nybin, ylow, yhigh, xtitle, ytitle):
    """
    Book a TH2F and return it
    """
    h = ROOT.TH2F(name, title, nxbin, xlow, xhigh, nybin, ylow, yhigh)
    h.GetXaxis().SetTitle(xtitle)
    h.GetYaxis().SetTitle(ytitle)
    h.Sumw2()
    return h

# ----------------------------------------------
#  TH3F Methods
# ----------------------------------------------
def th3f(name, title, nxbin, xlow, xhigh, nybin, ylow, yhigh, nzbin, zlow, zhigh, xtitle, ytitle, ztitle):
    """
    Book a TH3F and return it
    """
    h = ROOT.TH3F(name, title, nxbin, xlow, xhigh, nybin, ylow, yhigh, nzbin, zlow, zhigh)
    h.GetXaxis().SetTitle(xtitle)
    h.GetYaxis().SetTitle(ytitle)
    h.GetZaxis().SetTitle(ztitle)
    h.Sumw2()
    return h


# ----------------------------------------------
#  TProfile Methods
# ----------------------------------------------
def tprofile(name, title, nxbin, xlow, xhigh, ylow, yhigh, xtitle, ytitle, option=""):
    """
    Book a TProfile and return it
    """
    h = ROOT.TProfile(name, title, nxbin, xlow, xhigh, ylow, yhigh, option)
    h.GetXaxis().SetTitle(xtitle)
    h.GetYaxis().SetTitle(ytitle)
    h.Sumw2()
    return h


# ----------------------------------------------
#  TLegend Methods
# ----------------------------------------------
def default_legend(xl=0.7, yl=0.75, xh=0.9, yh=0.88):
    leg = ROOT.TLegend(xl, yl, xh, yh)
    # leg.SetNDC()
    leg.SetFillStyle(0)
    leg.SetFillColor(0)
    leg.SetBorderSize(0)
    leg.SetTextFont(42)
    return leg


# ----------------------------------------------
#  Text/Label Methods
# ----------------------------------------------
def draw_text(x=0.7, y=0.65, font=42, color=ROOT.kBlack, text="", size=0.04, angle=0.0):
    """
    Draw text on the current pad
    (coordinates are normalized to the current pad)
    """
    l = ROOT.TLatex()
    l.SetTextSize(size)
    l.SetTextFont(font)
    l.SetNDC()
    l.SetTextColor(color)
    l.SetTextAngle(angle)
    l.DrawLatex(x, y, text)


# ----------------------------------------------
#  Text Method
# ----------------------------------------------
def draw_text_on_top(text="", size=0.04, pushright=1.0, pushup=1.0):
    s = size
    t = text
    top_margin = ROOT.gPad.GetTopMargin()
    left_margin = ROOT.gPad.GetLeftMargin()
    xpos = pushright * left_margin
    ypos = 1.0 - 0.85 * top_margin
    ypos *= pushup
    # draw_text(x=xpos, y=1.0-0.85*top_margin, text=t, size=s)
    draw_text(x=xpos, y=ypos, text=t, size=s)


# ----------------------------------------------
#  TLine Methods
# ----------------------------------------------
def draw_line(xl=0.0, yl=0.0, xh=1.0, yh=1.0, color=ROOT.kBlack, width=2, style=1):
    l = ROOT.TLine(xl, yl, xh, yh)
    l.SetLineColor(color)
    l.SetLineWidth(width)
    l.SetLineStyle(style)
    l.Draw()


# ----------------------------------------------
#  Style Methods
# ----------------------------------------------
def set_palette(name="", ncontours=999):
    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue = [1.00, 0.84, 0.61, 0.34, 0.00]
    #    elif name == "redbluevector" :
    #        #stops = [0.00, 0.34, 0.61, 0.84, 1.00]
    #        stops = [0.00, 0.17, 0.61, 0.84, 1.00]
    #        #stops = [0.00, 0.20, 0.5, 0.70, 1.00]
    #        red   = [0.35, 0.29, 0.29, 0.89, 0.90]
    #        green = [0.70, 0.57, 0.35, 0.22, 0.05]
    #        blue  = [0.95, 0.88, 0.70, 0.45, 0.09]
    elif name == "redbluevector":
        stops = [0.09, 0.18, 0.27, 0.36, 0.45, 0.54, 0.63, 0.72, 0.81, 0.90, 1.00]
        red = [
            1 / 256.0,
            2 / 256.0,
            2 / 256.0,
            58 / 256.0,
            90 / 256.0,
            115 / 256.0,
            138 / 256.0,
            158 / 256.0,
            180 / 256.0,
            200 / 256.0,
            219 / 256.0,
        ]
        green = [
            158 / 256.0,
            150 / 256.0,
            140 / 256.0,
            131 / 256.0,
            120 / 256.0,
            110 / 256.0,
            97 / 256.0,
            82 / 256.0,
            62 / 256.0,
            34 / 256.0,
            2 / 256.0,
        ]
        blue = [
            237 / 256.0,
            224 / 256.0,
            213 / 256.0,
            200 / 256.0,
            190 / 256.0,
            178 / 256.0,
            167 / 256.0,
            156 / 256.0,
            146 / 256.0,
            134 / 256.0,
            123 / 256.0,
        ]
    elif name == "bluetowhite":
        stops = [0.0, 0.125, 0.25, 0.375, 0.5, 0.66, 0.83, 1.00, 1.00]
        red = [23 / 256.0, 46 / 256.0, 69 / 256.0, 92 / 256.0, 124 / 256.0, 157 / 256.0, 190 / 256.0, 222 / 256.0]
        green = [32 / 256.0, 63 / 256.0, 95 / 256.0, 126 / 256.0, 152 / 256.0, 178 / 256.0, 203 / 256.0, 229 / 256.0]
        blue = [57 / 256.0, 115 / 256.0, 172 / 256.0, 229 / 256.0, 235 / 256.0, 240 / 256.0, 245 / 256.0, 250 / 256.0]
    else:
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue = [0.51, 1.00, 0.12, 0.00, 0.00]
    s = array.array("d", stops)
    R = array.array("d", red)
    g = array.array("d", green)
    b = array.array("d", blue)
    npoints = len(s)
    ROOT.TColor.CreateGradientColorTable(npoints, s, R, g, b, ncontours)
    ROOT.gStyle.SetNumberContours(ncontours)


# ----------------------------------------------
#  ATLAS Style
# ----------------------------------------------
def atlasStyle():

    style = ROOT.TStyle("ATLAS", "ATLAS style")

    icol = ROOT.kWhite
    font = 42
    tsize = 0.03  # 0.05
    tlabelsize = 0.03
    markerStyle = 20
    msize = 1.2
    hlinewidth = 2

    # Canvas settings
    style.SetFrameBorderMode(icol)
    style.SetFrameFillColor(icol)
    style.SetCanvasBorderMode(icol)
    style.SetPadBorderMode(icol)
    style.SetPadColor(icol)
    style.SetCanvasColor(icol)
    style.SetStatColor(icol)

    # set the paper & margin sizes
    style.SetPaperSize(20, 26)
    style.SetPadTopMargin(0.15)  # 0.05
    style.SetPadRightMargin(0.15)  # 0.05
    style.SetPadBottomMargin(0.15)  # 0.16
    style.SetPadLeftMargin(0.15)  # 0.16

    # use large fonts
    style.SetTextFont(font)
    style.SetTextSize(tsize)

    style.SetTitleFont(font, "x")
    style.SetTitleFont(font, "y")
    style.SetTitleFont(font, "z")
    style.SetTitleOffset(2.0, "x")  # 1.4 #AT 0.9
    style.SetTitleOffset(2.0, "y")  # 1.4 #AT 1.5
    style.SetTitleOffset(2.0, "z")
    style.SetTitleSize(tsize, "x")
    style.SetTitleSize(tsize, "y")
    style.SetTitleSize(tsize, "z")
    style.SetTickLength(0.02, "x")
    style.SetTickLength(0.02, "y")
    style.SetTickLength(0.02, "z")

    style.SetLabelFont(font, "x")
    style.SetLabelFont(font, "y")
    style.SetLabelFont(font, "z")
    style.SetLabelOffset(0.02, "x")
    style.SetLabelOffset(0.02, "y")
    style.SetLabelOffset(0.02, "z")
    style.SetLabelSize(tlabelsize, "x")
    style.SetLabelSize(tlabelsize, "y")
    style.SetLabelSize(tlabelsize, "z")

    # palette settings
    # https://root.cern.ch/doc/master/classTColor.html
    style.SetPalette(ROOT.kBird)

    # use bold lines and markers
    style.SetMarkerStyle(markerStyle)
    style.SetMarkerSize(msize)
    style.SetHistLineWidth(hlinewidth)
    style.SetLineStyleString(2, "[12 12]")  # postscript dashes
    style.SetEndErrorSize(0.0)  # remove erro bar caps

    # do not display any of the standard histogram decorations
    style.SetStatX(0.99)
    style.SetStatY(0.99)
    style.SetStatH(0.01)
    style.SetStatW(0.2)

    style.SetStatStyle(0)
    style.SetStatFont(font)
    style.SetStatFontSize(0.03)
    style.SetOptStat("nemr")
    style.SetStatBorderSize(1)
    style.SetOptTitle(0)
    style.SetOptFit(0)

    style.SetTitleStyle(icol)
    style.SetTitleH(0.08)

    # put tick marks on top and RHS of plots
    style.SetPadTickX(1)
    style.SetPadTickY(1)

    return style
