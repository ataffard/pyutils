from setuptools import setup, find_packages


setup(
    name="myos",
    version="0.1",
    description="",
    long_description="Python module with OS Utilities",
    url="",
    author="Anyes Taffard",
    author_email="ataffard@cern.ch",
    packages=find_packages(),
    include_package_data=True,
    python_requires=">=3.6",
    install_requires=["requests"],
)
